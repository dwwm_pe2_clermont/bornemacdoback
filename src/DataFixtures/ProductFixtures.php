<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $bigMac = new Product();
        $bigMac->setNom("Big Mac");
        $bigMac->setImage("big-mac.webp");
        $bigMac->setPrix(8.95);
        $manager->persist($bigMac);

        $bigTasty = new Product();
        $bigTasty->setNom("Big Tasty");
        $bigTasty->setImage("bigtasty.png");
        $bigTasty->setPrix(9.99);
        $manager->persist($bigTasty);

        $filofish = new Product();
        $filofish->setNom("Filofish");
        $filofish->setImage("filofish.jpg");
        $filofish->setPrix(5);
        $manager->persist($filofish);

        $manager->flush();
    }
}
