<?php
namespace App\Controller;

use App\Entity\Book;
use App\Entity\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class CreateCommandController extends AbstractController
{

    public function __invoke(Command $command): Command
    {
        $command->setDateCommand(new \DateTime());
        $command->setUtilisateur($this->getUser());

        return $command;
    }
}
?>